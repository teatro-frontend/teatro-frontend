import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import axios from '../../api/axios';

// reactstrap components
import {
  Button,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
} from "reactstrap";

// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import LandingPageHeader from "components/Headers/LandingPageHeader.js";
import DefaultFooter from "components/Footers/DefaultFooter.js";

const GET_EVENTS_URL = "/eventos"

function LandingPage() {
  const [firstFocus, setFirstFocus] = useState(false);
  const [lastFocus, setLastFocus] = useState(false);

  const [events, setEvents] = useState([]);
  const [showItems] = useState(3);

  const getEvents = async () => {
    try {
      const response = await axios.get(GET_EVENTS_URL);
      
      response.data.map((item) => {        
        if (item.horarios.length === 0) {
          console.log(item)
          item.horarios.push({id : item.id,timestamp: 1658890800000})
        }
      }
    )
      setEvents(response.data.slice(0, showItems));
    } catch (error) {
      console.error(error);
    }
  }

  const getTime = (date) => {
    console.log(date)
    var timestamp = date
    var fecha = new Date(timestamp)
    return (fecha.getDate() +
      "/" + (fecha.getMonth() + 1) +
      "/" + fecha.getFullYear() +
      " " + fecha.getHours() +
      ":" + fecha.getMinutes())
  }

  useEffect(() => {
    document.body.classList.add("landing-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    getEvents();
    return function cleanup() {
      document.body.classList.remove("landing-page");
      document.body.classList.remove("sidebar-collapse");
    };
  }, []);

  return (
    <>
      <ExamplesNavbar />
      <div className="wrapper">
        <LandingPageHeader />
        <div className="section section-about-us">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto text-center" md="8">
                <h2 className="title">¿Quiénes somos?</h2>
                <h5 className="description">
                  El Teatro de la Universidad de Medellín, Gabriel Obregón Botero,
                  es un escenario perfecto para la realización de espectáculos y eventos
                  y se perfila como el Teatro de mayor calidad y avances tecnológicos en
                  la ciudad de Medellín, cuenta con  una acústica inigualable, excelente
                  visibilidad desde cualquier punto de la sala y una dotación que pondrá
                  sonido y colorido a sus eventos. Nuestro equipo humano y técnico brinda
                  un acompañamiento de principio a fin a instituciones, empresarios y particulares,
                  garantizando un evento de calidad memorable.
                </h5>
              </Col>
            </Row>
            <div className="separator separator-primary"></div>
            <div className="section-story-overview">
              <Row>
                <Col md="6">
                  <div
                    className="image-container image-left"
                    style={{
                      backgroundImage:
                        "url(" + require("assets/img/teatro.jpg").default + ")",
                    }}
                  >
                    <p className="blockquote blockquote-info">
                      "Aquí irá algo bueno que haya dicho alguien del teatro :D" <br></br>
                      <br></br>
                      <small>-Paulina</small>
                    </p>
                  </div>
                  <div
                    className="image-container"
                    style={{
                      backgroundImage:
                        "url(" + require("assets/img/teatro2.jpg").default + ")",
                    }}
                  ></div>
                </Col>
                <Col md="5">
                  <div
                    className="image-container image-right"
                    style={{
                      backgroundImage:
                        "url(" + require("assets/img/entrada.jpg").default + ")",
                    }}
                  ></div>
                  <h3>
                    Más de dos décadas de actividades en el sector cultural, académico y corporativo
                  </h3>
                  <p>
                    El Teatro de la Universidad de Medellín, inició actividades el 26 de septiembre
                    de 1985 y en homenaje a su principal impulsor, se le bautizó con el nombre de
                    "Gabriel Obregón Botero" en 1988. En un principio, el recinto era para uso
                    exclusivo de la Universidad, pero como un sueño lleno de color, queriendo
                    crear un lugar de magia y fantasía, que adornara y engalanara los eventos que
                    para ese entonces realizaba la Universidad, el Teatro se hizo cada vez más grande,
                    su infraestructura y la dinámica de proyección que alcanzó, hizo que rápidamente
                    abriera sus puertas a una ciudad ansiosa por vivir la cultura. Convirtiéndose así,
                    no sólo en el teatro más grande de Medellín, sino en el mejor escenario para los
                    artistas, las empresas y los espectáculos en la región, se ha convertido en la
                    mejor elección para los espectadores.
                  </p>
                  {/* <p>
                    For a start, it does not automatically follow that a record
                    amount of ice will melt this summer. More important for
                    determining the size of the annual thaw is the state of the
                    weather as the midnight sun approaches and temperatures
                    rise. But over the more than 30 years of satellite records,
                    scientists have observed a clear pattern of decline,
                    decade-by-decade.
                  </p>
                  <p>
                    The Arctic Ocean freezes every winter and much of the
                    sea-ice then thaws every summer, and that process will
                    continue whatever happens with climate change. Even if the
                    Arctic continues to be one of the fastest-warming regions of
                    the world, it will always be plunged into bitterly cold
                    polar dark every winter. And year-by-year, for all kinds of
                    natural reasons, there’s huge variety of the state of the
                    ice.
                  </p> */}
                </Col>
              </Row>
            </div>
          </Container>
        </div>
        <div className="section section-team text-center">
          <Container>
            <h2 className="title">Oferta de eventos</h2>
            <div className="team">
              <Row>
                {events?.map((event) => {
                  return (
                    <Col md="4">
                      <div className="team-player">
                        <img
                          style={{width: 100 + 'px', height: 100 + 'px'}}
                          alt="..."
                          className="rounded-circle img-fluid img-raised"
                          src={event.imagen}
                        ></img>
                        <h4 className="title">{event.artista}</h4>
                        <p className="category text-info">{getTime(event.horarios[0].timestamp)+"0"}</p>
                        <p className="description">
                          {event.nombre}
                          {/* <a href="#pablo" onClick={(e) => e.preventDefault()}>
                        links
                      </a>{" "} */}
                          {/* for people to be able to follow them outside the site. */}
                        </p>
                      </div>
                    </Col>
                  )
                })}
              </Row>
              <Row className="justify-content-center">
                <Link to="/view-event">
                  <Button color="primary" size="lg">Ver todos</Button>
                </Link>
              </Row>
            </div>
          </Container>
        </div>
        {/* <div className="section section-contact-us text-center">
          <Container>
            <h2 className="title">Want to work with us?</h2>
            <p className="description">Your project is very important to us.</p>
            <Row>
              <Col className="text-center ml-auto mr-auto" lg="6" md="8">
                <InputGroup
                  className={
                    "input-lg" + (firstFocus ? " input-group-focus" : "")
                  }
                >
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="now-ui-icons users_circle-08"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="First Name..."
                    type="text"
                    onFocus={() => setFirstFocus(true)}
                    onBlur={() => setFirstFocus(false)}
                  ></Input>
                </InputGroup>
                <InputGroup
                  className={
                    "input-lg" + (lastFocus ? " input-group-focus" : "")
                  }
                >
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="now-ui-icons ui-1_email-85"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="Email..."
                    type="text"
                    onFocus={() => setLastFocus(true)}
                    onBlur={() => setLastFocus(false)}
                  ></Input>
                </InputGroup>
                <div className="textarea-container">
                  <Input
                    cols="80"
                    name="name"
                    placeholder="Type a message..."
                    rows="4"
                    type="textarea"
                  ></Input>
                </div>
                <div className="send-button">
                  <Button
                    block
                    className="btn-round"
                    color="info"
                    href="#pablo"
                    onClick={(e) => e.preventDefault()}
                    size="lg"
                  >
                    Send Message
                  </Button>
                </div>
              </Col>
            </Row>
          </Container>
        </div> */}
        <DefaultFooter />
      </div>
    </>
  );
}

export default LandingPage;
