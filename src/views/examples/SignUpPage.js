import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import axios from '../../api/axios';
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
} from "reactstrap";

const USER_REGEX = /^[A-z][A-z0-9-_]{3,23}$/;
//const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/;
//const PHONE_REGEX = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const REGISTER_URL = '/usuarios'
// core components



function SignUpPage() {
  const userRef = useRef();
  const errRef = useRef();

  const [user, setUser] = useState('');
  const [validName, setValidName] = useState(false);
  const [userFocus, setUserFocus] = useState(false);

  const [email, setEmail] = useState('');
  const [validEmail, setValidEmail] = useState(false);
  const [emailFocus, setEmailFocus] = useState(false);

  const [id, setId] = useState('');
  const [validId, setValidId] = useState(false);
  const [idFocus, setIdFocus] = useState(false);

  const [phone, setPhone] = useState('');
  const [validPhone, setValidPhone] = useState(false);
  const [phoneFocus, setPhoneFocus] = useState(false);

  const [password, setPassword] = useState('');
  const [validPassword, setValidPassword] = useState(false);
  const [passwordFocus, setPasswordFocus] = useState(false);

  const [matchPwd, setMatchPwd] = useState('');
  const [validMatch, setValidMatch] = useState(false);
  const [matchFocus, setMatchFocus] = useState(false);

  const [errMsg, setErrMsg] = useState('');
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    userRef.current.focus();
  }, [])

  useEffect(() => {
    setValidName(USER_REGEX.test(user));
  }, [user])

 /*  useEffect(() => {
    setValidPhone(PHONE_REGEX.test(phone));
  }, [phone]) */

  useEffect(() => {
    setValidEmail(EMAIL_REGEX.test(email));
  }, [email])

  useEffect(() => {
    if(errMsg !== '') {
      alert(errMsg)
    }    
  }, [errMsg])

  useEffect(() => {
    //setValidPassword(PWD_REGEX.test(password));
    setValidMatch(password === matchPwd);
  }, [password, matchPwd])

  useEffect(() => {
    setErrMsg('');
  }, [user, password, matchPwd])

  const handleSignUp = async () => {/* 
    // if button enabled with JS hack
    const v1 = USER_REGEX.test(user);
    const v2 = PWD_REGEX.test(password);
    if (!v1 || !v2) {
      setErrMsg("Invalid Entry");
      return;
    } */
    try {
      const response = await axios.post(REGISTER_URL,
        JSON.stringify({ nombre: user, identificacion: id, correo: email, celular: phone }),
        {
          headers: { 'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*' },
        }
      );
      console.log(response.data)
      console.log(JSON.stringify(response))
      setSuccess(true);
      //clear input fields
    } catch (error) {
      if(!error?.response) {
        setErrMsg('No hay respuesta del servidor')
      } else if(error.response?.status == 409){
        //user taken
      } else {
        setErrMsg('El registro falló')
      }
    }
  }

  return (
    <>
      <div
        className="section section-signup"
        style={{
          backgroundImage:
            "url(" + require("assets/img/teatro3.jpg").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center",
          minHeight: "700px",
        }}
      >
        <Container>
          <Row>
            <Card className="card-signup" data-background-color="blue">
              {/* <p ref={errRef} className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p> */}
              <Form action="" className="form" method="">
                <CardHeader className="text-center">
                  <CardTitle className="title-up" tag="h3">
                    Registro
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <InputGroup
                    className={
                      "no-border" + (userFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons users_circle-08"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Nombre..."
                      type="text"
                      ref={userRef}
                      value={user}
                      required
                      aria-invalid={validName ? "false" : "true"}
                      onChange={(e) => setUser(e.target.value)}
                      onFocus={() => setUserFocus(true)}
                      onBlur={() => setUserFocus(false)}
                    ></Input>
                  </InputGroup>
                  {userFocus && user && !validName && (
                    <p className={userFocus && user && !validName ? "instructions" : "offscreen"}>
                      Solo se permiten letras
                    </p>
                  )}
                  <InputGroup
                    className={
                      "no-border" + (emailFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons ui-1_email-85"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Correo..."
                      type="email"
                      value={email}
                      required
                      onChange={(e) => setEmail(e.target.value)}
                      onFocus={() => setEmailFocus(true)}
                      onBlur={() => setEmailFocus(false)}
                    ></Input>
                  </InputGroup>
                  {emailFocus && email && !validEmail && (
                    <p className={emailFocus && email && !validEmail ? "instructions" : "offscreen"}>
                      El formato del correo no es correcto
                    </p>
                  )}
                  <InputGroup
                    className={
                      "no-border" + (idFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons business_badge"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Identificación..."
                      type="number"
                      value={id}
                      required
                      onChange={(e) => setId(e.target.value)}
                      onFocus={() => setIdFocus(true)}
                      onBlur={() => setIdFocus(false)}
                    ></Input>
                  </InputGroup>
                  <InputGroup
                    className={
                      "no-border" + (phoneFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons tech_mobile"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Telefono..."
                      type="phone"
                      value={phone}
                      onChange={(e) => setPhone(e.target.value)}
                      onFocus={() => setPhoneFocus(true)}
                      onBlur={() => setPhoneFocus(false)}
                    ></Input>
                  </InputGroup>
                  <InputGroup
                    className={
                      "no-border" + (passwordFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons ui-1_lock-circle-open"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Contraseña"
                      type="password"
                      required
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      onFocus={() => setPasswordFocus(true)}
                      onBlur={() => setPasswordFocus(false)}
                    ></Input>
                  </InputGroup>
                  <InputGroup
                    className={
                      "no-border" + (matchFocus ? " input-group-focus" : "")
                    }
                  >
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="now-ui-icons ui-1_lock-circle-open"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      required
                      value={matchPwd}
                      placeholder="Repita la contraseña"
                      type="password"
                      onChange={(e) => setMatchPwd(e.target.value)}
                      onFocus={() => setMatchFocus(true)}
                      onBlur={() => setMatchFocus(false)}
                    ></Input>
                  </InputGroup>
                  {matchFocus && !validMatch && (
                    <p className={matchFocus && !validMatch ? "instructions" : "offscreen"}>
                      Las contraseñas deben de coincidir
                    </p>
                  )}
                </CardBody>
                <CardFooter className="text-center">
                  <Button
                    className="btn-neutral btn-round"
                    color="info"
                    onClick={handleSignUp}
                    size="lg"
                    disabled={!validName || !validMatch || !validEmail ? true : false}
                  >
                    Registrarse
                  </Button>
                </CardFooter>
              </Form>
            </Card>
          </Row>
          <div className="col text-center">
            <Link to="/login-page">
              <Button
                className="btn-round btn-white"
                color="primary"
                size="lg"
              >
                Inicio de sesión
              </Button>
            </Link>
          </div>
        </Container>
      </div>
    </>
  );
}

export default SignUpPage;
