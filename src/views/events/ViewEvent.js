import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import axios from '../../api/axios';

// reactstrap components
import {
  Badge,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Col,
  UncontrolledTooltip,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
} from "reactstrap";

// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import LandingPageHeader from "components/Headers/LandingPageHeader";
import DefaultFooter from "components/Footers/DefaultFooter.js";

const GET_EVENTS_URL = "/eventos"

function CreateEvent() {
  const [events, setEvents] = useState([]);

  const getEvents = async () => {
    try {
      const response = await axios.get(GET_EVENTS_URL);
      
      response.data.map((item) => {        
          if (item.horarios.length === 0) {
            console.log(item)
            item.horarios.push({id : item.id,timestamp: 1658890800000})
          }
        }
      )
      console.log(response.data)
      setEvents(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  React.useEffect(() => {
    document.body.classList.add("view-event");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    getEvents();
    return function cleanup() {
      document.body.classList.remove("view-event");
      document.body.classList.remove("sidebar-collapse");
    };
  }, []);

  const getTime = (date) => {
    if (date != 0) {
      var timestamp = date
      var fecha = new Date(timestamp)
      return (fecha.getDate() +
        "/" + (fecha.getMonth() + 1) +
        "/" + fecha.getFullYear() +
        " " + fecha.getHours() +
        ":" + fecha.getMinutes())
    } else {
      return "4/6/2022 15:0"
    }

  }
  return (
    <>
      <ExamplesNavbar />
      <div className="wrapper">
        <LandingPageHeader />
        <div className="section text-center">
          <Container>
            <h2 className="title">Oferta de Eventos</h2>
            <Row>
              {events.map((event) => {
                return (
                  <Col className="d-flex align-items-stretch" md="4" key={event.id}>
                    <Card className="card" data-background-color="blue">
                      <CardHeader className="text-center">
                        <img src={event.imagen} className="card-img-top" alt="..."></img>
                      </CardHeader>
                      <CardBody>
                        <CardTitle className="mr-auto ml-auto" tag="h4">
                          {event.nombre}
                        </CardTitle>
                        <p className="category text-info">{getTime(event.horarios[0].timestamp) + "0"}</p>
                        <Row className="justify-content-center">
                          <Badge color="default" className="mr-1">
                            {event.categoria.nombre}
                          </Badge>
                          <Badge color="default" className="mr-1">
                            {event.genero.nombre}
                          </Badge>
                        </Row>

                        {/* <p className="description">
                          {event.descripcion}
                        </p> */}
                      </CardBody>
                      <CardFooter className="text-center">
                        <Link to={`/info-event/${event.id}`}>
                          <Button
                            className="btn-round btn-white"
                            color="primary"
                            size="lg"
                          >
                            Ver más
                          </Button>
                        </Link>
                        <Link to={`/edit-event/${event.id}`}>
                          <Button
                            className="btn-round btn-white"
                            color="primary"
                            size="lg"
                          >
                            Editar
                          </Button>
                        </Link>
                      </CardFooter>
                    </Card>
                  </Col>
                )
              })}
            </Row>
          </Container>
        </div>
        <DefaultFooter />
      </div>
    </>
  );
}

export default CreateEvent;
