import React, { useState, useEffect, useRef } from "react";
import { Link, useParams } from "react-router-dom";
import axios from '../../api/axios';

// reactstrap components
import {
    Badge,
    Col,
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Container,
    Row,
} from "reactstrap";

// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import LandingPageHeader from "components/Headers/LandingPageHeader";
import DefaultFooter from "components/Footers/DefaultFooter.js";

const GET_EVENTS_URL = "/eventos"
const EVENTS = []

function InfoEvent(props) {
    const { id } = useParams()
    const [event, setEvent] = useState({
        imagen: "",
        organizador: {
            numero_puleb: ""
        },
        responsable: {
            numero_puleb: ""
        },
        horarios: [
            { timestamp: "" }
        ],
        artista: "",
        nombre: "",
        categoria: {
            nombre: ""
        },
        genero: {
            nombre: ""
        }
    });

    const getEvent = async () => {
        try {
            const response = await axios.get(`${GET_EVENTS_URL}/${id}`);
            setEvent(response.data[0]);
        } catch (error) {
            console.error(error);
        }
    }

    const getTime = (date) => {
        var timestamp = date
        var fecha = new Date(timestamp)
        return (fecha.getDate() +
            "/" + (fecha.getMonth() + 1) +
            "/" + fecha.getFullYear() +
            " " + fecha.getHours() +
            ":" + fecha.getMinutes())
    }

    useEffect(() => {
        document.body.classList.add("view-event");
        document.body.classList.add("sidebar-collapse");
        document.documentElement.classList.remove("nav-open");
        window.scrollTo(0, 0);
        document.body.scrollTop = 0;
        getEvent();
        return function cleanup() {
            document.body.classList.remove("view-event");
            document.body.classList.remove("sidebar-collapse");
        };
    }, []);
    return (
        <>
            <ExamplesNavbar />
            <div className="wrapper">
                <LandingPageHeader />
                <div className="section text-center">
                    <Container>
                        <h2 className="title">Información del evento</h2>
                        <Row>
                            <Col className="" md="6">
                                <img src={event.imagen} alt="..."></img>
                            </Col>
                            <Col className="" md="6" >
                                <Card className="card" data-background-color="blue">
                                    <CardBody>
                                        <CardTitle className="mr-auto ml-auto" tag="h3">
                                            {event.nombre}
                                        </CardTitle>
                                        <Row className="justify-content-center">
                                            <Badge color="default" className="mr-1">
                                                {event.categoria.nombre}
                                            </Badge>
                                            <Badge color="default" className="mr-1">
                                                {event.genero.nombre}
                                            </Badge>
                                        </Row>
                                        <p className="category text-info">Fecha y hora: {getTime(event.horarios[0].timestamp)+"0"}</p>
                                        <p className="category text-info">Artista: {event.artista}</p>
                                        <p className="category text-black">PULEP del organizador: {event.organizador.numero_puleb}</p>
                                        <p className="category text-black">PULEP del responsable: {event.responsable.numero_puleb}</p>
                                        <p className="description">
                                            { }
                                        </p>
                                    </CardBody>
                                    <CardFooter className="text-center">
                                        <Link to={`/edit-event/${event.id}`}>
                                            <Button
                                                className="btn-round btn-white"
                                                color="primary"
                                                size="lg"
                                            >
                                                Editar
                                            </Button>
                                        </Link>
                                    </CardFooter>
                                </Card>
                            </Col>

                        </Row>
                        <h2 className="title">Plano de la sala</h2>
                        <Row className="justify-content-center">
                            <img src="https://udem.edu.co/images/SERVICIOS/Teatro/Imagenes/Portafolio/planosala.jpg" alt="..."></img>

                        </Row>
                        <Button className="btn-round" color="primary" size="lg">Adquirir entradas</Button>
                    </Container>
                </div>
                <DefaultFooter />
            </div>
        </>
    );
}

export default InfoEvent;
