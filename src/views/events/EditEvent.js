import React, { useState, useEffect, useRef } from "react";
import { Redirect, useParams } from "react-router-dom";
import axios from '../../api/axios';

// reactstrap components
import {
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  Input,
  CustomInput,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  FormGroup,
  Label
} from "reactstrap";

// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import LandingPageHeader from "components/Headers/LandingPageHeader";
import DefaultFooter from "components/Footers/DefaultFooter.js";

const EVENT_URL = '/eventos'
const style = { color: '#000000' }
const GET_CATEGORIES_URL = '/intereses'
const GET_INVOLUCRADOS_URL = '/involucrados'
const REDIRECT_PAGE_URL = '/info-event'

function CreateEvent() {
  const nameRef = useRef();

  const [redirect, setRedirect] = useState(false)

  const { id } = useParams()

  const [name, setName] = useState('');
  const [nameFocus, setNameFocus] = useState(false);

  const [artista, setArtista] = useState('');
  const [artistaFocus, setArtistaFocus] = useState(false);

  const [genero, setGenero] = useState('');
  const [generos, setGeneros] = useState([]);
  const [generoFocus, setGeneroFocus] = useState(false);

  const [categoria, setCategoria] = useState('');
  const [categorias, setCategorias] = useState([]);
  const [categoriaFocus, setCategoriaFocus] = useState(false);

  const [responsable, setResponsable] = useState('');
  const [responsables, setResponsables] = useState([]);
  const [responsableFocus, setResponsableFocus] = useState(false);

  const [organizador, setOrganizador] = useState('');
  const [organizadores, setOrganizadores] = useState([]);
  const [organizadorFocus, setOrganizadorFocus] = useState(false);

  const handleSave = async () => {
    try {
      console.log(JSON.stringify({ nombre: name, artista, genero, categoria, responsable, organizador }))
      const response = await axios.put(`${EVENT_URL}/${id}`,
        JSON.stringify({ nombre: name, artista, genero, categoria, responsable, organizador }),
        {
          headers: { 'Content-Type': 'application/json' },
        }
      );      
      console.log(response.data)
      alert('Evento editado correctamente')
      setRedirect(true)
      //console.log(response.data)      
    } catch (error) {
      if (!error?.response) {
        setRedirect(true)
      } else {
        alert('El registro del evento falló')
      }
    }
  }

  const obtenerCategorias = async () => {
    try {
      const response = await axios.get(GET_CATEGORIES_URL,
        {
          headers: { 'Content-Type': 'application/json' },
        }
      );

      response.data.map((item) => {
        if (item.tipo === 'Genero') {
          setCategorias(oldArray => [...oldArray, item])
        } else {
          setGeneros(oldArray => [...oldArray, item])
        }
      })

    } catch (error) {
      if (!error?.response) {
        //setErrMsg('No hay respuesta del servidor')
      } else {
        //setErrMsg('Error obteniendo las categorías')
      }
    }
  }

  const obtenerInvolucrados = async () => {
    try {
      const response = await axios.get(GET_INVOLUCRADOS_URL,
        {
          headers: { 'Content-Type': 'application/json' },
        }
      );
      response.data.map((item) => {
        if (item.rol === 'Organ') {
          setOrganizadores(oldArray => [...oldArray, item])
        } else {
          setResponsables(oldArray => [...oldArray, item])
        }
      })
    } catch (error) {
      if (!error?.response) {
        //setErrMsg('No hay respuesta del servidor')
      } else {
        //setErrMsg('Error obteniendo las categorías')
      }
    }
  }

  const getEvent = async () => {
    try {
      const response = await axios.get(`${EVENT_URL}/${id}`);
      const event = response.data[0]

      setArtista(event.artista)
      setName(event.nombre)
      setGenero(event.genero.id)
      setCategoria(event.categoria.id)
      setOrganizador(event.organizador.id)
      setResponsable(event.responsable.id)
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    document.body.classList.add("create-event");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    nameRef.current.focus();
    getEvent()
    obtenerCategorias()
    obtenerInvolucrados()
    return function cleanup() {
      document.body.classList.remove("create-event");
      document.body.classList.remove("sidebar-collapse");
    };
  }, []);

  if (redirect) {
    return <Redirect to={`${REDIRECT_PAGE_URL}/${id}`} />;
  }

  return (
    <>
      <ExamplesNavbar />
      <div className="wrapper">
        <LandingPageHeader />
        <div className="section">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" md="7">
                <Card className="card" data-background-color="blue">
                  <Form action="" className="form" method="">
                    <CardHeader className="text-center mt-5">
                      <CardTitle className="" tag="h2">
                        Edición del Evento
                      </CardTitle>
                    </CardHeader>
                    <CardBody>
                      <FormGroup>
                        <Label for="nombre">Nombre del evento *</Label>
                        <InputGroup
                          className={
                            "no-border" + (nameFocus ? " input-group-focus" : "")
                          }
                          md="6"
                        >
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons users_circle-08"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            id="nombre"
                            placeholder="Nombre"
                            type="text"
                            ref={nameRef}
                            value={name}
                            required
                            onChange={(e) => setName(e.target.value)}
                            onFocus={() => setNameFocus(true)}
                            onBlur={() => setNameFocus(false)}
                          ></Input>
                        </InputGroup>
                      </FormGroup>

                      <FormGroup>
                        <Label for="artista">Nombre del artista *</Label>
                        <InputGroup
                          className={
                            "no-border" + (artistaFocus ? " input-group-focus" : "")
                          }
                        >
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons design_palette"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            id="artista"
                            placeholder="Artista"
                            type="text"
                            value={artista}
                            required
                            onChange={(e) => setArtista(e.target.value)}
                            onFocus={() => setArtistaFocus(true)}
                            onBlur={() => setArtistaFocus(false)}
                          ></Input>
                        </InputGroup>
                      </FormGroup>


                      <FormGroup>
                        <Label for="responsablesSelect">Seleccione el responsable *</Label>
                        <InputGroup className={
                          "no-border" + (responsableFocus ? " input-group-focus" : "")
                        }>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons business_badge"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            name="responsables"
                            id="responsablesSelect"
                            onFocus={() => setResponsableFocus(true)}
                            onBlur={() => setResponsableFocus(false)}
                            value={responsable}
                            onChange={(e) => setResponsable(e.target.value)}>
                            {responsables?.map((item) => {
                              return (
                                <option style={style} key={item.id} value={item.id}>{item.nombre} : {item.numero_puleb}</option>
                              )
                            })}
                          </Input>
                        </InputGroup>
                      </FormGroup>


                      <FormGroup>
                        <Label for="organizadoresSelect">Seleccione el organizador *</Label>
                        <InputGroup className={
                          "no-border" + (organizadorFocus ? " input-group-focus" : "")
                        }>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons business_badge"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            name="organizadores"
                            id="organizadoresSelect"
                            onFocus={() => setOrganizadorFocus(true)}
                            onBlur={() => setOrganizadorFocus(false)}
                            value={organizador}
                            onChange={(e) => setOrganizador(e.target.value)}>
                            {organizadores?.map((item) => {
                              return (
                                <option style={style} key={item.id} value={item.id}>{item.nombre} : {item.numero_puleb}</option>
                              )
                            })}
                          </Input>
                        </InputGroup>
                      </FormGroup>

                      <FormGroup>
                        <Label for="categoriasSelect">Seleccione la categoría *</Label>
                        <InputGroup className={
                          "no-border" + (categoriaFocus ? " input-group-focus" : "")
                        }>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons business_globe"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            name="categorias"
                            id="categoriasSelect"
                            onFocus={() => setCategoriaFocus(true)}
                            onBlur={() => setCategoriaFocus(false)}
                            value={categoria}
                            onChange={(e) => setCategoria(e.target.value)}>
                            {categorias?.map((item) => {
                              return (
                                <option style={style} key={item.id} value={item.id}>{item.nombre}</option>
                              )
                            })}
                          </Input>
                        </InputGroup>
                      </FormGroup>

                      <FormGroup>
                        <Label for="generosSelect">Seleccione el género *</Label>
                        <InputGroup className={
                          "no-border" + (generoFocus ? " input-group-focus" : "")
                        }>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons business_globe"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            name="generos"
                            id="generosSelect"
                            onFocus={() => setGeneroFocus(true)}
                            onBlur={() => setGeneroFocus(false)}
                            value={genero}
                            onChange={(e) => setGenero(e.target.value)}
                          >

                            {generos?.map((item) => {
                              return (
                                <option style={style} key={item.id} value={item.id}>{item.nombre}</option>
                              )
                            })}
                          </Input>
                        </InputGroup>
                      </FormGroup>                     

                    </CardBody>
                    <CardFooter className="text-center">
                      <Button
                        className="btn-neutral btn-round"
                        color="info"
                        onClick={handleSave}
                        size="lg"
                        disabled={!name || !artista || !genero || !categoria || !organizador || !responsable ? true : false}
                      >
                        Actualizar Evento
                      </Button>
                    </CardFooter>
                  </Form>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
        <DefaultFooter />
      </div>
    </>
  );
}

export default CreateEvent;
