import axios from 'axios';

export default axios.create({
    baseURL: 'https://teatro-backend.herokuapp.com/api'
});